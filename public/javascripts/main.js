$(document).ready(() => { 
    $('.deleteuser').on('click', () => { 
        event.preventDefault(); // stop it from following the link
        var confirmation = confirm('Are you sure that you want to delete this user?');
        if (confirmation) {
            $.ajax({
                type: 'DELETE',
                url: '/user/'+$('.deleteuser').data('user')
            }).done(
                (response) => { 
                    window.location.replace('/users');
                }
            );
        }
        else {
            return false;
        }
    });
})