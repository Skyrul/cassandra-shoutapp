var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');
var client = new cassandra.Client({
  contactPoints: ['127.0.0.1'],
  localDataCenter: 'datacenter1',
  keyspace: 'shoutapp'
});
client.connect((err, result) => { 
  console.log('cassandra connected: users');
});

var getAllUsers = 'SELECT * FROM shoutapp.users';

/* GET users listing. */
router.get('/', function(req, res, next) {
  // res.send('respond with a resource');
  client.execute(getAllUsers, [], (err, result) => {
    if (err) {
      res.status(404).send({ msg: err });
    }
    else {
      // res.json(result);
      res.render('users', {
        users: result.rows
      });
    }
  });
});

module.exports = router;
