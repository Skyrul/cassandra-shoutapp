var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');
var client = new cassandra.Client({
  contactPoints: ['127.0.0.1'],
  localDataCenter: 'datacenter1',
  keyspace: 'shoutapp'
});
client.connect((err, result) => { 
  console.log('cassandra connected: user');
});

var getByUsername = 'SELECT * FROM shoutapp.users WHERE username = ?;';

router.get('/:username', (req, res) => {
    client.execute(getByUsername, [req.params.username], (err, result) => { 
        if (err) {
            res.status(404).send({ msg: err });
        }
        else {
            res.render('user', {
                username: result.rows[0].username,
                email: result.rows[0].email,
                full_name: result.rows[0].full_name,
                id: result.rows[0].id
            });
        }
    });
});

var deleteUser = "DELETE FROM shoutapp.users WHERE username = ?";

router.delete('/:username', (req, res) => { 
    client.execute(deleteUser, [req.params.username], (err, result) => {
        if (err) {
            res.status(404).send({ msg: err });
        }
        else {
            res.json(result);
        }
    });
});

module.exports = router;