var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');
var client = new cassandra.Client({
  contactPoints: ['127.0.0.1'],
  localDataCenter: 'datacenter1',
  keyspace: 'shoutapp'
});
client.connect((err, result) => { 
  console.log('cassandra connected: shouts');
});

var getAllShouts = 'SELECT * FROM shoutapp.shouts';

router.get('/', (req, res) => {
    client.execute(getAllShouts, [], (err, result) => { 
        if (err) {
            res.status(404).send({ msg: err });
        }
        else {
            res.render('shouts', {
                shouts: result.rows
            });
        }
    });
});

// query for usershout

var getUserShouts = "SELECT * FROM shoutapp.usershouts WHERE username = ? ALLOW FILTERING";

router.get('/:username', (req, res) => {
    client.execute(getUserShouts, [req.params.username], (err, result) => {
        if (err) {
            res.status(404).send({ msg: err });
        } else {
            res.render('shouts', {
                shouts: result.rows
            });
        }
    });
});

module.exports = router;