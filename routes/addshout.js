var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');
var client = new cassandra.Client({
  contactPoints: ['127.0.0.1'],
  localDataCenter: 'datacenter1',
  keyspace: 'shoutapp'
});
client.connect((err, result) => { 
  console.log('cassandra connected: addshout');
});

const { v4: uuidv4 } = require('uuid');


router.post('/', (req, res) => { 
    var id1 = cassandra.types.Uuid.random()
    var id2 = cassandra.types.TimeUuid.now()

    var queries = [
        {
            query: 'INSERT INTO shoutapp.shouts(shout_id, username, body) VALUES(?,?,?)',
            params: [id2, req.body.username, req.body.body]
        },
        {
            query: 'INSERT INTO shoutapp.usershouts(username, shout_id, body, id) VALUES(?,?,?,?)',
            params: [req.body.username, id2, req.body.body, id1]
        }
    ];
    queryOptions = {};
    client.batch(queries, queryOptions, (err) => {         
        console.log(err);
        res.redirect('/shouts');
    });
});

module.exports = router;