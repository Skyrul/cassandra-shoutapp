var express = require('express');
var router = express.Router();
var cassandra = require('cassandra-driver');
var client = new cassandra.Client({
  contactPoints: ['127.0.0.1'],
  localDataCenter: 'datacenter1',
  keyspace: 'shoutapp'
});
client.connect((err, result) => { 
  console.log('cassandra connected: adduser');
});

const { v4: uuidv4 } = require('uuid');


var getByUsername = 'SELECT * FROM shoutapp.users WHERE username = ?';

router.get('/:username', (req, res)=>{ 
    client.execute(getByUsername, [req.params.username], (err, result) => { 
        if (err) {
            res.status(404).send({ msg: err });
        }
        else {
            res.render('edituser', {
                username: result.rows[0].username,
                email: result.rows[0].email,
                full_name: result.rows[0].full_name,
                password: result.rows[0].password,
                id: result.rows[0].id,
            });
        }
    });
});

var upsertUser = 'INSERT INTO shoutapp.users(username, password, email, full_name, id) VALUES(?,?,?,?,?)';

router.post('/', (req, res) => { 
    client.execute(upsertUser, [req.body.username, req.body.password, req.body.email, req.body.full_name, uuidv4()], (err, result) => {
        if (err) {
            res.status(404).send({ msg: err });
        }
        else {
            console.log('User Updated');
            res.redirect('/users/' + req.body.username);
        }
    });
});

module.exports = router;